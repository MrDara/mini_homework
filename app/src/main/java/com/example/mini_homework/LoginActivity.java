package com.example.mini_homework;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.example.mini_homework.R;
import com.example.mini_homework.flash.FirstScreenFragment;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FirstScreenFragment firstScreenFragment = new FirstScreenFragment();

        loadFragment(firstScreenFragment);
    }

    private void loadFragment(FirstScreenFragment firstScreenFragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
        transaction.add(R.id.container_fragment_login,firstScreenFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }
}