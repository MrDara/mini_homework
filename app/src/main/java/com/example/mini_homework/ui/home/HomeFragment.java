package com.example.mini_homework.ui.home;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Adapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.mini_homework.R;
import com.example.mini_homework.map.GetNearbyPlacesDate;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class HomeFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener, View.OnClickListener {

    private HomeViewModel homeViewModel;
    View fragment;
    FusedLocationProviderClient client;

    SearchView searchView;


    private GoogleMap mMap;

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private LocationListener listener;
    private long UPDATE_INTERVAL = 2000;
    private long FASTEST_INTERVAL = 5000;
    private LocationManager locationManager;
    private LatLng latLng;
    private boolean isPermission;

    SupportMapFragment supportMapFragment;

    int i = 1;

    Button btnRestaurant, btnGasStation, btnBank, btnSeatFlat;
    HorizontalScrollView horizontalScrollView;

    FusedLocationProviderClient fusedLocationProviderClient;

    int PROXIMITY_RADIUS = 1000;
    double latitude,longitude;

    @Override
    public void onStart() {
        super.onStart();
        String name = getArguments().getString("user");
        String mail = getArguments().getString("email");
        Toast.makeText(getContext(), "Result : " + name + " : " + mail, Toast.LENGTH_LONG).show();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
//        final TextView textView = root.findViewById(R.id.text_view);
//        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });

        fragment = root.findViewById(R.id.google_map);
        searchView = root.findViewById(R.id.search_view);

        if (requestSinglePermission()) {

            supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.google_map);
            supportMapFragment.getMapAsync(this);

            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

            checkLocation();

        }

        setInitLayout(root);
        setSearchView();

        return root;
    }

    // for init
    public void setInitLayout(View view) {
//        btnRestaurant = view.findViewById(R.id.btn_restaurant);
//        btnBank = view.findViewById(R.id.btn_bank);
//        btnSeatFlat = view.findViewById(R.id.btn_seat_flat);
//        btnGasStation = view.findViewById(R.id.btn_gas_station);
//        horizontalScrollView = view.findViewById(R.id.scrollView);
        // method for set listener
//        setListenerForButton();
        // set layout


    }

    // set Listener for button
    private void setListenerForButton() {
//        btnRestaurant.setOnClickListener(this);
//        btnGasStation.setOnClickListener(this);
//        btnSeatFlat.setOnClickListener(this);
//        btnBank.setOnClickListener(this);
    }


    private boolean checkLocation() {

        if (!isLocationEnabled()) {
            ShowAlert();
        }

        return isLocationEnabled();
    }

    private void ShowAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Enable Location")
                .setMessage("Your Location Setting is set to 'OFF'. \n Please Enable Location to use this app")
                .setPositiveButton("Location Setting", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.show();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

    }

    private boolean requestSinglePermission() {

        Dexter.withContext(getContext())
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                        isPermission = true;
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                        if (permissionDeniedResponse.isPermanentlyDenied()) {
                            isPermission = false;
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {

                    }
                }).check();
        return isPermission;
    }

    // for search location
    private void setSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                 String location = searchView.getQuery().toString();
                 List<Address> addressList = null;
                 if (location != null || !location.equals("")){
                     Geocoder geocoder = new Geocoder(getContext());
                     try {

                         addressList = geocoder.getFromLocationName(location,1);
                         if (addressList == null){
                            Toast.makeText(getContext(),"Not Found location!",Toast.LENGTH_LONG).show();
                         }else{
                             try {
                                 Address address = addressList.get(0);
                                 Log.d("address",address.toString());
                                 latLng = new LatLng(address.getLatitude(),address.getLongitude());
                                 mMap.addMarker(new MarkerOptions().position(latLng).title(location));
                                 mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
                             }catch (IndexOutOfBoundsException e){
                                 Toast.makeText(getContext(),"Location not found!",Toast.LENGTH_LONG).show();
                             }
                         }
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        startLocationUpdate();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdate();
        } else {
//            Toast.makeText(getContext(), "Location not Deleted", Toast.LENGTH_LONG).show();
        }

    }

    private void startLocationUpdate() {

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mMap.clear();
//        String msg = "Updated Location : "+ Double.toString(location.getLatitude())+" , "+Double.toString(location.getLongitude());
//        Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();

        latLng = new LatLng(location.getLatitude(), location.getLongitude());

        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

//        mMap.getUiSettings().setZoomControlsEnabled(true);
//        mMap.getUiSettings().setZoomGesturesEnabled(true);
//        mMap.getUiSettings().setCompassEnabled(true);
//        mMap.setPadding(0,0,0,0);

        if (latLng != null) {
            Log.d("TAG", "X , Y: " + latLng);
            mMap.addMarker(new MarkerOptions().position(latLng).title("Marker is Current Location"));
            if (i == 1) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15F));
            }
            if (i == 2147483647) {

                i = 1;
            }
            i++;

        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    // set on Click listener
    @Override
    public void onClick(View v) {
//        Object dataTransfer[] = new Object[2];
//        GetNearbyPlacesDate getNearbyPlacesDate = new GetNearbyPlacesDate();
//        switch (v.getId()) {
//            case R.id.btn_restaurant:
//
//                mMap.clear();
//                String restaurant = "restaurant";
//                String url = getUrl(latitude,longitude,restaurant);
//                dataTransfer[0] = mMap;
//                dataTransfer[1] = url;
//
//
//                getNearbyPlacesDate.execute(dataTransfer);
//                Toast.makeText(getContext(), "Showing Nearby restaurants", Toast.LENGTH_LONG).show();
//                break;
//            case R.id.btn_gas_station:
//
//                mMap.clear();
//                String gas_station = "gas_station";
//                url = getUrl(latitude,longitude,gas_station);
//                dataTransfer[0] = mMap;
//                dataTransfer[1] = url;
//
//
//                getNearbyPlacesDate.execute(dataTransfer);
//
//                Toast.makeText(getContext(), "gas_station", Toast.LENGTH_LONG).show();
//                break;
//            case R.id.btn_bank:
//                mMap.clear();
//                String bank = "bank";
//                url = getUrl(latitude,longitude,bank);
//                dataTransfer[0] = mMap;
//                dataTransfer[1] = url;
//                getNearbyPlacesDate.execute(dataTransfer);
//                Toast.makeText(getContext(), "bank", Toast.LENGTH_LONG).show();
//                break;
//            case R.id.btn_seat_flat:
//                Toast.makeText(getContext(), "seat_flat", Toast.LENGTH_LONG).show();
//                break;
//        }
    }


    private String getUrl(double latitude,double longitude,String nearbyPlace){


        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location" + latitude +","+longitude);
        googlePlaceUrl.append("&radius="+PROXIMITY_RADIUS);
        googlePlaceUrl.append("&type="+nearbyPlace);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key="+"AIzaSyCS73jZZiUpZq7imQ9QQSeSSK16-r4ZjHg");

        return googlePlaceUrl.toString();
    }


}