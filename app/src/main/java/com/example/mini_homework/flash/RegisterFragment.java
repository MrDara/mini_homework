package com.example.mini_homework.flash;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.PatternMatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mini_homework.MainActivity;
import com.example.mini_homework.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;


public class RegisterFragment extends Fragment implements View.OnClickListener{

    private Button btnRegister;
    private TextView txtNewName,txtRegister;

    protected EditText editEmail,editPwd,editName;

    private FirebaseAuth mAuth;
    FirebaseUser currentUser;
    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_register, container, false);

        btnRegister = view.findViewById(R.id.btn_Register);
        btnRegister.setOnClickListener(this);
        txtNewName = view.findViewById(R.id.txt_new_user);
        txtRegister = view.findViewById(R.id.txt_Register_user);
        txtRegister.setOnClickListener(this);
        txtNewName.setOnClickListener(this);

        editEmail = view.findViewById(R.id.edit_email);
        editName = view.findViewById(R.id.edit_user_name);
        editPwd = view.findViewById(R.id.edit_password);


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_Register:
                registerUser();
                break;
            case R.id.txt_new_user:
                LogInFragment fragment = new LogInFragment();
                backFragment(fragment);
                break;
            case R.id.txt_Register_user:
                RegisterFragment backFragment = new RegisterFragment();
                replaceFragment(backFragment);
                break;
        }
    }

    private void registerUser() {

      final String name = editName.getText().toString();
      final String pwd = editPwd.getText().toString();
      final String email = editEmail.getText().toString();

      if (name.isEmpty()){
          editName.setError("Full name is requested !");
          editName.requestFocus();
          return;
      }
      if (email.isEmpty()){
          editEmail.setError("Email is requested!");
          editEmail.requestFocus();
          return;
      }
      if (pwd.isEmpty()){
          editEmail.setError("Password is requested!");
          editEmail.requestFocus();
          return;
      }
      if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
          editEmail.setError("Please provide valid email");
          editEmail.requestFocus();
          return;
      }

      mAuth.createUserWithEmailAndPassword(email,pwd)
              .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                  @Override
                  public void onComplete(@NonNull Task<AuthResult> task) {
                     if (task.isSuccessful()){
                         User user = new User(name,pwd,email);

                         FirebaseDatabase.getInstance().getReference("Users")
                                 .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                 .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                             @Override
                             public void onComplete(@NonNull Task<Void> task) {
                                 if (task.isSuccessful()){
                                     Toast.makeText(getContext(),"User has been registered successfully",Toast.LENGTH_LONG).show();
                                     Intent intent = new Intent(getContext(), MainActivity.class);
                                     intent.putExtra("email",email);
                                     intent.putExtra("password",pwd);
                                     intent.putExtra("user",name);
                                     startActivity(intent);
                                 }else{
                                     Toast.makeText(getContext(),"Failed to register! Try again",Toast.LENGTH_LONG).show();
                                 }
                             }
                         });

                     }
                  }
              });




    }

    private void backFragment(LogInFragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container_fragment_login, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void replaceFragment(RegisterFragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container_fragment_login, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }



}