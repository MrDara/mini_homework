package com.example.mini_homework.flash;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mini_homework.MainActivity;
import com.example.mini_homework.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class LogInFragment extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private Button btnSigIn;
    private TextView txtNewName,txtRegister;

    private ImageView btnGoogle,btnFaceBook;

    protected EditText email,password;

    FirebaseAuth mAuth;
    FirebaseUser firebaseUser;
    DatabaseReference reference;
    String UserID;

    final String[] userName = new String[1];
    final String[] emailOne = new String[1];
    final String[] pwdOne = new String[1];

    private GoogleApiClient googleApiClient;
    private static final  int SIGN_IN = 1;

    public LogInFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_log_in, container, false);

        initializeLayout(view);
        setListener();

        return view;
    }

    private void initializeLayout(View view){
        btnSigIn = view.findViewById(R.id.btn_SignIn);
        txtNewName = view.findViewById(R.id.txt_new_user);
        txtRegister = view.findViewById(R.id.txt_Register_user);
        email = view.findViewById(R.id.edit_email);
        password = view.findViewById(R.id.edit_password);
        btnGoogle = view.findViewById(R.id.btn_google);
    }

    private void setListener(){
        btnSigIn.setOnClickListener(this);
        txtRegister.setOnClickListener(this);
        txtNewName.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_SignIn:
                signIn();
                break;
            case R.id.txt_new_user:
                LogInFragment fragment = new LogInFragment();
                backFragment(fragment);
                break;
            case R.id.txt_Register_user:
                RegisterFragment backFragment = new RegisterFragment();
                replaceFragment(backFragment);
                break;
            case R.id.btn_google:
                googleSingIn();
                break;
        }
    }

    // Authorization Firebase.
    private void signIn() {
        boolean validation;
        validation = validationEmailAndPassword(email.getText().toString(),password.getText().toString());
        if (validation == true){


            mAuth.signInWithEmailAndPassword(email.getText().toString(),password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){

                        signInAndPassData();

                    }else {
                        Toast.makeText(getContext(),"Failed to login! Please check your credentials",Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }else{
            Toast.makeText(getContext(),"Invalid Input",Toast.LENGTH_LONG).show();
            return;
        }


    }
    // Get Data form Firebase.
    private void signInAndPassData() {

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        UserID = firebaseUser.getUid();

        reference.child(UserID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);
                if (user !=null){
                    userName[0] = user.getName();
                    emailOne[0] = user.getEmail();
                    pwdOne[0] = user.getPassword();
                    transferToMainActivity();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getContext(),"Failed to login! Please check your credentials",Toast.LENGTH_LONG).show();
            }
        });

    }

    // form transfer to MainActivity.
    private void transferToMainActivity(){
        Intent intent = new Intent(getContext(),MainActivity.class);
        intent.putExtra("email",emailOne[0]);
        intent.putExtra("password", pwdOne[0]);
        intent.putExtra("user",userName[0]);
        startActivity(intent);
    }

    private void backFragment(LogInFragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container_fragment_login, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void replaceFragment(RegisterFragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container_fragment_login, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private boolean validationEmailAndPassword(String email, String password){

        if(email == null || password == null || email.isEmpty() || password.isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    // method form login with google.
    private void googleSingIn(){
        GoogleSignInOptions  gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(getContext()).enableAutoManage(getActivity(),this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso).build();
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent,SIGN_IN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            if (result.isSuccess()){
                GoogleSignInAccount account = result.getSignInAccount();
                Toast.makeText(getContext(),"Google Email",Toast.LENGTH_LONG).show();
                if (account !=null){
                    Intent intent = new Intent(getContext(),MainActivity.class);
                    intent.putExtra("email",account.getEmail());
                    intent.putExtra("password", account.getAccount());
                    intent.putExtra("user",account.getDisplayName());
                    intent.putExtra("image",account.getPhotoUrl().toString());
//                    Log.d("image",account.getPhotoUrl().toString());
                    startActivity(intent);
                }else {
                    Toast.makeText(getContext(),"Google Email Failed",Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    // method for login with google.
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}